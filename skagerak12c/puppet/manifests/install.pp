Package{allow_virtual => false,}

node 'default' {

  include os
  include ssh
  include java
  
  include orawls::weblogic, orautils
  include fmw
  
  Class[java] -> Class[orawls::weblogic] -> Class[fmw]

}

# operating settings for Middleware
class os {

  $default_params = {}
  $host_instances = hiera('hosts', {})
  create_resources('host',$host_instances, $default_params)

  class { 'swap_file':
    swapfile     => '/u01/swap/swap.1',
    swapfilesize => '8192000000'
  }

  service { iptables:
        enable    => false,
        ensure    => false,
        hasstatus => true,
  }

  group { 'dba' :
    ensure => present,
  }

  # http://raftaman.net/?p=1311 for generating password
  # password = oracle
  user { 'oracle' :
    ensure     => present,
    groups     => 'dba',
    shell      => '/bin/bash',
    password   => '$1$DSJ51vh6$4XzzwyIOk6Bi/54kglGk3.',
    home       => "/home/oracle",
    comment    => 'wls user created by Puppet',
    managehome => true,
    require    => Group['dba'],
  }

  $install = [ 'binutils.x86_64','unzip.x86_64']


  package { $install:
    ensure  => present,
  }

  class { 'limits':
    config => {
               '*'       => {  'nofile'  => { soft => '2048'   , hard => '8192',   },},
               'oracle'  => {  'nofile'  => { soft => '65536'  , hard => '65536',  },
                               'nproc'   => { soft => '2048'   , hard => '16384',   },
                               'memlock' => { soft => '1048576', hard => '1048576',},
                               'stack'   => { soft => '10240'  ,},},
               },
    use_hiera => false,
  }

  sysctl { 'kernel.msgmnb':                 ensure => 'present', permanent => 'yes', value => '65536',}
  sysctl { 'kernel.msgmax':                 ensure => 'present', permanent => 'yes', value => '65536',}
  sysctl { 'kernel.shmmax':                 ensure => 'present', permanent => 'yes', value => '2588483584',}
  sysctl { 'kernel.shmall':                 ensure => 'present', permanent => 'yes', value => '2097152',}
  sysctl { 'fs.file-max':                   ensure => 'present', permanent => 'yes', value => '6815744',}
  sysctl { 'net.ipv4.tcp_keepalive_time':   ensure => 'present', permanent => 'yes', value => '1800',}
  sysctl { 'net.ipv4.tcp_keepalive_intvl':  ensure => 'present', permanent => 'yes', value => '30',}
  sysctl { 'net.ipv4.tcp_keepalive_probes': ensure => 'present', permanent => 'yes', value => '5',}
  sysctl { 'net.ipv4.tcp_fin_timeout':      ensure => 'present', permanent => 'yes', value => '30',}
  sysctl { 'kernel.shmmni':                 ensure => 'present', permanent => 'yes', value => '4096', }
  sysctl { 'fs.aio-max-nr':                 ensure => 'present', permanent => 'yes', value => '1048576',}
  sysctl { 'kernel.sem':                    ensure => 'present', permanent => 'yes', value => '250 32000 100 128',}
  sysctl { 'net.ipv4.ip_local_port_range':  ensure => 'present', permanent => 'yes', value => '9000 65500',}
  sysctl { 'net.core.rmem_default':         ensure => 'present', permanent => 'yes', value => '262144',}
  sysctl { 'net.core.rmem_max':             ensure => 'present', permanent => 'yes', value => '4194304', }
  sysctl { 'net.core.wmem_default':         ensure => 'present', permanent => 'yes', value => '262144',}
  sysctl { 'net.core.wmem_max':             ensure => 'present', permanent => 'yes', value => '1048576',}

}

class ssh {
  require os


  # file { "/home/oracle/.ssh/":
  #   owner  => "oracle",
  #   group  => "dba",
  #   mode   => "700",
  #   ensure => "directory",
  #   alias  => "oracle-ssh-dir",
  # }

  # file { "/home/oracle/.ssh/id_rsa.pub":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "644",
  #   source  => "/vagrant/ssh/id_rsa.pub",
  #   require => File["oracle-ssh-dir"],
  # }

  # file { "/home/oracle/.ssh/id_rsa":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "600",
  #   source  => "/vagrant/ssh/id_rsa",
  #   require => File["oracle-ssh-dir"],
  # }

  # file { "/home/oracle/.ssh/authorized_keys":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "644",
  #   source  => "/vagrant/ssh/id_rsa.pub",
  #   require => File["oracle-ssh-dir"],
  # }
}

class java {
  require os

  $remove = [ "java-1.7.0-openjdk.x86_64", "java-1.6.0-openjdk.x86_64" ]

  package { $remove:
    ensure  => absent,
  }

  include jdk7

  jdk7::install7{ 'jdk-8u101-linux-x64':
      version                     => "8u101" ,
      full_version                => "jdk1.8.0_101",
      alternatives_priority       => 18000,
      x64                         => true,
      download_dir                => "/u01/tmp/install",
      urandom_java_fix            => true,
      rsa_key_size_fix            => true,
      cryptography_extension_file => "jce_policy-8.zip",
      source_path                 => "/u01/workspace/software",
  }
}

class fmw{
  require orawls::weblogic
  $default_params = {}
  $fmw_installations = hiera('fmw_installations', {})
  create_resources('orawls::fmw',$fmw_installations, $default_params)
}
