Package{allow_virtual => false,}

node 'default' {

  include pack_domain

}

# operating settings for Middleware
class os {

  $default_params = {}
  $host_instances = hiera('hosts', {})
  create_resources('host',$host_instances, $default_params)

  class { 'swap_file':
    swapfile     => '/u01/swap/swap.1',
    swapfilesize => '8192000000'
  }

  service { iptables:
        enable    => false,
        ensure    => false,
        hasstatus => true,
  }

  group { 'dba' :
    ensure => present,
  }

  # http://raftaman.net/?p=1311 for generating password
  # password = oracle
  user { 'oracle' :
    ensure     => present,
    groups     => 'dba',
    shell      => '/bin/bash',
    password   => '$1$DSJ51vh6$4XzzwyIOk6Bi/54kglGk3.',
    home       => "/home/oracle",
    comment    => 'wls user created by Puppet',
    managehome => true,
    require    => Group['dba'],
  }

  $install = [ 'binutils.x86_64','unzip.x86_64']


  package { $install:
    ensure  => present,
  }

  class { 'limits':
    config => {
               '*'       => {  'nofile'  => { soft => '2048'   , hard => '8192',   },},
               'oracle'  => {  'nofile'  => { soft => '65536'  , hard => '65536',  },
                               'nproc'   => { soft => '2048'   , hard => '16384',   },
                               'memlock' => { soft => '1048576', hard => '1048576',},
                               'stack'   => { soft => '10240'  ,},},
               },
    use_hiera => false,
  }

  sysctl { 'kernel.msgmnb':                 ensure => 'present', permanent => 'yes', value => '65536',}
  sysctl { 'kernel.msgmax':                 ensure => 'present', permanent => 'yes', value => '65536',}
  sysctl { 'kernel.shmmax':                 ensure => 'present', permanent => 'yes', value => '2588483584',}
  sysctl { 'kernel.shmall':                 ensure => 'present', permanent => 'yes', value => '2097152',}
  sysctl { 'fs.file-max':                   ensure => 'present', permanent => 'yes', value => '6815744',}
  sysctl { 'net.ipv4.tcp_keepalive_time':   ensure => 'present', permanent => 'yes', value => '1800',}
  sysctl { 'net.ipv4.tcp_keepalive_intvl':  ensure => 'present', permanent => 'yes', value => '30',}
  sysctl { 'net.ipv4.tcp_keepalive_probes': ensure => 'present', permanent => 'yes', value => '5',}
  sysctl { 'net.ipv4.tcp_fin_timeout':      ensure => 'present', permanent => 'yes', value => '30',}
  sysctl { 'kernel.shmmni':                 ensure => 'present', permanent => 'yes', value => '4096', }
  sysctl { 'fs.aio-max-nr':                 ensure => 'present', permanent => 'yes', value => '1048576',}
  sysctl { 'kernel.sem':                    ensure => 'present', permanent => 'yes', value => '250 32000 100 128',}
  sysctl { 'net.ipv4.ip_local_port_range':  ensure => 'present', permanent => 'yes', value => '9000 65500',}
  sysctl { 'net.core.rmem_default':         ensure => 'present', permanent => 'yes', value => '262144',}
  sysctl { 'net.core.rmem_max':             ensure => 'present', permanent => 'yes', value => '4194304', }
  sysctl { 'net.core.wmem_default':         ensure => 'present', permanent => 'yes', value => '262144',}
  sysctl { 'net.core.wmem_max':             ensure => 'present', permanent => 'yes', value => '1048576',}

}

class ssh {
  require os


  # file { "/home/oracle/.ssh/":
  #   owner  => "oracle",
  #   group  => "dba",
  #   mode   => "700",
  #   ensure => "directory",
  #   alias  => "oracle-ssh-dir",
  # }

  # file { "/home/oracle/.ssh/id_rsa.pub":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "644",
  #   source  => "/vagrant/ssh/id_rsa.pub",
  #   require => File["oracle-ssh-dir"],
  # }

  # file { "/home/oracle/.ssh/id_rsa":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "600",
  #   source  => "/vagrant/ssh/id_rsa",
  #   require => File["oracle-ssh-dir"],
  # }

  # file { "/home/oracle/.ssh/authorized_keys":
  #   ensure  => present,
  #   owner   => "oracle",
  #   group   => "dba",
  #   mode    => "644",
  #   source  => "/vagrant/ssh/id_rsa.pub",
  #   require => File["oracle-ssh-dir"],
  # }
}

class java {
  require os

  $remove = [ "java-1.7.0-openjdk.x86_64", "java-1.6.0-openjdk.x86_64" ]

  package { $remove:
    ensure  => absent,
  }

  include jdk7

  jdk7::install7{ 'jdk-8u65-linux-x64':
      version                     => "8u65" ,
      full_version                => "jdk1.8.0_65",
      alternatives_priority       => 18000,
      x64                         => true,
      download_dir                => "/u01/tmp/install",
      urandom_java_fix            => true,
      rsa_key_size_fix            => true,
      cryptography_extension_file => "jce_policy-8.zip",
      source_path                 => "/u01/workspace/software",
  }
}

class fmw{
  require orawls::weblogic
  $default_params = {}
  $fmw_installations = hiera('fmw_installations', {})
  create_resources('orawls::fmw',$fmw_installations, $default_params)
}

class opatch{
  
  require fmw,orawls::weblogic
  orawls::opatch {'21830665':
      ensure                  => "present",
      oracle_product_home_dir => "/opt/oracle/middleware12c",
      jdk_home_dir            => "/usr/java/latest",
      patch_id                => "21830665",
      patch_file              => "p21830665_122100_Generic.zip",
      os_user                 => "oracle",
      os_group                => "dba",
      download_dir            => "/u01/tmp/install",
      source                  => "/u01/workspace/software",
      log_output              => false,
    }

  orawls::opatch {'22331568':
      ensure                  => "present",
      oracle_product_home_dir => "/opt/oracle/middleware12c",
      jdk_home_dir            => "/usr/java/latest",
      patch_id                => "22331568",
      patch_file              => "p22331568_122100_Generic.zip",
      os_user                 => "oracle",
      os_group                => "dba",
      download_dir            => "/u01/tmp/install",
      source                  => "/u01/workspace/software",
      log_output              => false,
      require                 => Orawls::Opatch['21830665'],
    }

}

class domains{
  require orawls::weblogic, opatch

  $default_params = {}
  $domain_instances = hiera('domain_instances', {})
  create_resources('orawls::domain',$domain_instances, $default_params)

  $wls_setting_instances = hiera('wls_setting_instances', {})
  create_resources('wls_setting',$wls_setting_instances, $default_params)

}

class pack_domain{
  $domain_name  = hiera('domain_name','')
  $jdk_home_dir = "/usr/java/latest"
  $exec_path    = "${jdk_home_dir}/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin"  
  $packCommand  = "/u01/oracle/middleware12211c/oracle_common/common/bin/pack.sh -domain=/u01/oracle/wlsdomains/domains/${domain_name} -template=/u02/templates/${domain_name}.jar -template_name=${domain_name} -log=/u02/templates/${domain_name}.log"
  
  exec { "pack domain  ${domain_name}":
    command     => $packCommand,
    creates     => "/u02/templates/${domain_name}.jar",
    path        => $exec_path,
    user        => "oracle",
    group       => "dba",
    logoutput   => true,
    environment => "JAVA_HOME=${jdk_home_dir}",
  }

}

class unpack_domain{
  $domain_name  = hiera('domain_name','')
  $jdk_home_dir = "/usr/java/latest"
  $exec_path    = "${jdk_home_dir}/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin"

  $unPackCommand = "/opt/oracle/middleware12c/oracle_common/common/bin/unpack.sh -domain=/opt/oracle/wlsdomains/domains/${domain_name} -template=/opt/oracle/wlsdomains/domains/${domain_name}.jar -app_dir=/opt/oracle/wlsdomains/applications/${domain_name} -log=/opt/oracle/wlsdomains/${domain_name}.log -log_priority=INFO"

     exec { "unpack ${domain_name}":
        command => "${unPackCommand} -user_name=weblogic -password=weblogic1",
        path        => $exec_path,
        user        => "oracle",
        group       => "dba",
        logoutput   => true,
        environment => "JAVA_HOME=${jdk_home_dir}",
     }
}
