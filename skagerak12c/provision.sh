#!/bin/bash
if [ $# -ge 1 ]
then
    ARG1=${1}
    echo "option selected:" $ARG1
    puppet apply --verbose --trace --modulepath ./puppet/modules --hiera_config ./puppet/hiera.yaml ./puppet/manifests/$ARG1.pp
else
    echo "wrong number of arguments ( install | patch | domain | dbadapters | datasources | pack | unpack )"
fi

